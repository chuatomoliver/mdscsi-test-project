package com.goldendevs.tugis.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.goldendevs.tugis.Activity.Home;
import com.goldendevs.tugis.Activity.SignIn;
import com.goldendevs.tugis.R;

import customfonts.MyTextView_Roboto_Regular;

public class HomePager3 extends Fragment {
     MyTextView_Roboto_Regular button;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.item_viewpage_3,container,false);
        button = (MyTextView_Roboto_Regular)view.findViewById(R.id.btnStart);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SignIn.class));
            }
        });
        return view;
    }

}
