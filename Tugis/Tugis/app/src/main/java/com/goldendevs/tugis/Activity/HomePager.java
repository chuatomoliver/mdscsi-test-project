package com.goldendevs.tugis.Activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.goldendevs.tugis.Adapter.HomePageAdapter;
import com.goldendevs.tugis.R;

import me.relex.circleindicator.CircleIndicator;

public class HomePager extends AppCompatActivity  {

    private ViewPager v1;
    private HomePageAdapter a;
    private CircleIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_pager);
        v1 = (ViewPager)findViewById(R.id.v1);
        CircleIndicator indicator = (CircleIndicator)findViewById(R.id.indicator);
        a = new HomePageAdapter(getSupportFragmentManager());
        v1.setAdapter(a);
        indicator.setViewPager(v1);
        a.registerDataSetObserver(indicator.getDataSetObserver());
    }


}
