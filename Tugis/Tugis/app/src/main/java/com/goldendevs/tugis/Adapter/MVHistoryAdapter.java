package com.goldendevs.tugis.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.goldendevs.tugis.Model.MVHistoryObject;
import com.goldendevs.tugis.R;

import java.util.List;

public class    MVHistoryAdapter extends BaseAdapter {
    private List<MVHistoryObject> mCartItemObjects;

    private LayoutInflater mInflater;
    private Context mContext;

    public MVHistoryAdapter(Context context, List<MVHistoryObject> objects) {
        this.mContext = context;
        this.mCartItemObjects = objects;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mCartItemObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return mCartItemObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = mInflater.inflate(R.layout.list_item_mv_history, null);

        TextView txtname = (TextView) view.findViewById(R.id.text);
        TextView txtlocation = (TextView) view.findViewById(R.id.location);
        TextView txtphone = (TextView) view.findViewById(R.id.date);



        txtname.setText(mCartItemObjects.get(i).getName());
        txtlocation.setText(mCartItemObjects.get(i).getLocation());
        txtphone.setText(mCartItemObjects.get(i).getPhone());

        return view;
    }
}
