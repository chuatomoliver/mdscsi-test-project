package com.goldendevs.tugis.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.goldendevs.tugis.Model.DrugtestObject;
import com.goldendevs.tugis.R;

import java.util.List;

public class DrugtestAdapter extends BaseAdapter {
    private List<DrugtestObject> mCartItemObjects;

    private LayoutInflater mInflater;
    private Context mContext;

    public DrugtestAdapter(Context context, List<DrugtestObject> objects) {
        this.mContext = context;
        this.mCartItemObjects = objects;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mCartItemObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return mCartItemObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = mInflater.inflate(R.layout.list_item_drugtest, null);

        TextView txtClinic = (TextView) view.findViewById(R.id.text);
        TextView txtStatus = (TextView) view.findViewById(R.id.status);
        TextView txtDate = (TextView) view.findViewById(R.id.date);


        txtClinic.setText(mCartItemObjects.get(i).getClinic());
        txtStatus.setText(mCartItemObjects.get(i).getStatus());
        txtDate.setText(mCartItemObjects.get(i).getDate());
        if(mCartItemObjects.get(i).getStatus().equalsIgnoreCase("pending")){
            txtStatus.getResources().getColor(R.color.warning);
        }
        if(mCartItemObjects.get(i).getStatus().equalsIgnoreCase("reject")){
            txtStatus.getResources().getColor(R.color.red);
        }
        return view;
    }
}
