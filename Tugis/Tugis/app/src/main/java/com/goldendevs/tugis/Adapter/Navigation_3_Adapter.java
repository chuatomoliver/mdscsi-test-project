package com.goldendevs.tugis.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goldendevs.tugis.Activity.FingerPrint;
import com.goldendevs.tugis.Activity.Home;
import com.goldendevs.tugis.Activity.MVhistory;
import com.goldendevs.tugis.Activity.Profile;
import com.goldendevs.tugis.Activity.SignIn;
import com.goldendevs.tugis.Activity.Task;
import com.goldendevs.tugis.Model.Nav3ModelClass;
import com.goldendevs.tugis.R;

/**
 * Created by praja on 23-05-17.
 */

public class Navigation_3_Adapter extends RecyclerView.Adapter<Navigation_3_Adapter.MyViewHolder>{


public Context mContext;
    public Nav3ModelClass[] nav_3_modelClasses;

    public Navigation_3_Adapter(Context mContext, Nav3ModelClass[] nav_3_modelClasses) {
        this.mContext = mContext;
        this.nav_3_modelClasses = nav_3_modelClasses;
    }

public class MyViewHolder extends RecyclerView.ViewHolder{

    public TextView txtname,txtcount;
    public ImageView img;

    public MyViewHolder(View itemView) {
        super(itemView);

        txtname = (TextView)itemView.findViewById(R.id.txtname);
        txtcount = (TextView)itemView.findViewById(R.id.txtcount);
        img = (ImageView)itemView.findViewById(R.id.img);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                if (pos == 0){
                    Intent intent = new Intent (v.getContext(), Profile.class);
                    v.getContext().startActivity(intent);
                }
                if (pos == 1){
                    Intent intent = new Intent (v.getContext(), Home.class);
                    v.getContext().startActivity(intent);
                    ((Activity)mContext).finish();
                }

                if (pos == 2){
                    Intent intent = new Intent (v.getContext(), FingerPrint.class);
                    v.getContext().startActivity(intent);
//                    ((Activity)mContext).finish();
                }
                if (pos == 3){
                    Intent intent = new Intent (v.getContext(), Task.class);
                    v.getContext().startActivity(intent);
//                    ((Activity)mContext).finish();
                }

                if (pos == 4){
                    Intent intent = new Intent (v.getContext(), MVhistory.class);
                    v.getContext().startActivity(intent);
//                    ((Activity)mContext).finish();
                }

                if (pos == 6){
                    Intent intent = new Intent (v.getContext(), SignIn.class);
                    v.getContext().startActivity(intent);
                    ((Activity)mContext).finish();
                }

            }
        });



    }
}


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nav_3, parent, false);
        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtname.setText(nav_3_modelClasses[position].getName());
        holder.txtcount.setText(nav_3_modelClasses[position].getCount());
        holder.img.setImageResource(nav_3_modelClasses[position].getImage());


        if (position == 3){

            holder.txtcount.setBackground(mContext.getResources().getDrawable(R.drawable.round_rect));
        }else if (position == 4){
            holder.txtcount.setBackground(mContext.getResources().getDrawable(R.drawable.round_rect));
        }
        else if (position == 5){
            holder.txtcount.setBackground(mContext.getResources().getDrawable(R.drawable.round_rect));
        }

    }

    @Override
    public int getItemCount() {
        return nav_3_modelClasses.length;
    }


}
