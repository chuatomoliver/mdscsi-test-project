package com.goldendevs.tugis.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.goldendevs.tugis.R;

import customfonts.MyTextView_Roboto_Regular;

public class SignIn extends Base {

    MyTextView_Roboto_Regular btnSign;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        InitVariables();
        btnSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Home.class));
                finish();
            }
        });

        logo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("API URL");
                builder.setMessage("Enter API URL: 192.168.1.2:80");

                final EditText input = new EditText(v.getContext());
                input.setText(getApiUrl());
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setMaxLines(1);

                builder.setView(input);

                builder.setNeutralButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String apiUrl = input.getText().toString();
                        SharedPreferences sharedPref = getSharedPreferences("API_URL", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("API_URL", apiUrl);
                        editor.commit();
                    }
                });

                builder.show();
                return false;
            }
        });

    }

    public void InitVariables(){
        btnSign = (MyTextView_Roboto_Regular)findViewById(R.id.btnSignIn);
        logo = (ImageView)findViewById(R.id.logo);
    }
}
