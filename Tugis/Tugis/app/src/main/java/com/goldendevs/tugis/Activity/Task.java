package com.goldendevs.tugis.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.goldendevs.tugis.Adapter.DrugtestAdapter;
import com.goldendevs.tugis.Adapter.TaskAdapter;
import com.goldendevs.tugis.Model.DrugtestObject;
import com.goldendevs.tugis.Model.TaskObject;
import com.goldendevs.tugis.R;
import com.goldendevs.tugis.app.App;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Task extends Base {

    private List<TaskObject> mDrugList = new ArrayList<>();
    private ListView lv_items;
    ImageView navigation_back;
    TaskAdapter mAdapter;
    Context context = this;
    FloatingActionButton btnRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        InitVariables();
        getDrughistory();
        navigation_back .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void InitVariables(){
        navigation_back = (ImageView)findViewById(R.id.navigation_back);
        lv_items = (ListView)findViewById(R.id.list_items);

    }

    public void getDrughistory() {

        StringRequest postRequest = new StringRequest(Request.Method.GET, "http://"+getApiUrl()+""+ENDPOINT+"getTask", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Toast.makeText(context, "response", Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(response);
                    if(obj.getString("error").equals("false")) {
                        JSONArray jsonArray = obj.getJSONArray("details");
                        for (int x = 0; x < jsonArray.length(); x++) {
                            JSONObject object = jsonArray.getJSONObject(x);
                            TaskObject userDetailsObject = new TaskObject();
                            userDetailsObject.setPoliceid(object.getString("police_id"));
                            userDetailsObject.setTask(object.getString("task"));
                            userDetailsObject.setStatus(object.getString("status"));
                            mDrugList.add(userDetailsObject);

//                          User
                        } // end for request
                        mAdapter = new TaskAdapter(context, mDrugList);
                        lv_items.setAdapter(mAdapter);
                    }
                    else{
                        Toast.makeText(context, "Please check username and passw", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Iwatcher", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
//                params.put("username", username);
//                params.put("password", password);
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }
}
