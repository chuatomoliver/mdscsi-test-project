package com.goldendevs.tugis.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.goldendevs.tugis.R;
import com.goldendevs.tugis.app.App;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import customfonts.EditText__SF_Pro_Display_Light;
import customfonts.MyTextView_Roboto_Regular;

public class MVReport extends Base {

    final int CAMERA_CAPTURE = 1;
    //captured picture uri
    private Uri picUri;
    ImageView imageview;
    TextView btnGetImage, btn_show;
    TextView tv_settext;
    Bitmap selectedImage;

    TextView mBtnCapturetalaga;
    Context context = this;
    // ttospeech
    TextToSpeech t1;

    String mFilenamePhotoID;
    StorageReference mStorageRef;
    MyTextView_Roboto_Regular btnSubmit;
    Bitmap rotatedBitmap = null;
    ImageView back;

    String path;
    EditText__SF_Pro_Display_Light name;
    EditText__SF_Pro_Display_Light phonenumber;
    EditText__SF_Pro_Display_Light location;
    EditText__SF_Pro_Display_Light notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvreport);
        InitVariables();

        mBtnCapturetalaga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                File imagesFolder = new File(Environment.getExternalStorageDirectory(), "platenumber/");
                imagesFolder.mkdirs();
                mFilenamePhotoID = "id" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
                File image = new File(imagesFolder, mFilenamePhotoID);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
                startActivityForResult(cameraIntent, 2);
            }
        });

        btnGetImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });


        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    Locale locale = new Locale("tl_PH","Philippines");
                    Locale.setDefault(locale);
                    t1.setLanguage(locale);
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Home.class));
                finish();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Upload();


        }
        });
    }



    public void SaveMvRecords() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://"+getApiUrl()+ ENDPOINT +"saveMvReport", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("resp123",""+response);
                    JSONObject obj = new JSONObject(response);
                    Upload();
                    if(obj.getString("error").equals("false")) {
                        Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Toast.makeText(context, "Please check username and passw", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Response123Error", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("path", path);
                params.put("name", name.getText().toString());
                params.put("phone", phonenumber.getText().toString());
                params.put("location", location.getText().toString());
                params.put("notes", notes.getText().toString());
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }






    private void Upload(){
        final ProgressDialog progressDialog =  new ProgressDialog(this);
        progressDialog.setTitle("Upload Photo....");
        progressDialog.show();
        Uri file = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/platenumber/" + mFilenamePhotoID));
        final StorageReference riversRef = mStorageRef.child("/platenumber/" + mFilenamePhotoID);
        riversRef.putFile(file).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // Get a URL to the uploaded content
                riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        path = uri.toString();
                        Log.d("URL", "onSuccess: uri= "+ path);
                        SaveMvRecords();
                    }
                });
                Log.d("upload","success");
                progressDialog.dismiss();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Log.d("upload","failed");
                        Toast.makeText(context, "" + exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                        progressDialog.setMessage(((int)progress) + "% Uploaded ...");

                    }
                });
    }










    private void InitVariables(){
        btnGetImage = (TextView)findViewById(R.id.capture_btn);
        imageview = (ImageView)findViewById(R.id.picture);
        mBtnCapturetalaga = (TextView)findViewById(R.id.camera_btn);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        btnSubmit = (MyTextView_Roboto_Regular) findViewById(R.id.btnSubmit);
        tv_settext = (TextView)findViewById(R.id.txtSetText);
        back = (ImageView)findViewById(R.id.navigation_back);

        name = (EditText__SF_Pro_Display_Light)findViewById(R.id.edt_name);
        phonenumber = (EditText__SF_Pro_Display_Light)findViewById(R.id.edt_phone);
        location = (EditText__SF_Pro_Display_Light)findViewById(R.id.edt_location);
        notes = (EditText__SF_Pro_Display_Light)findViewById(R.id.edt_notes);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                selectedImage = BitmapFactory.decodeStream(imageStream);
                Log.d("bitmap get image" , " " + selectedImage);

                TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                if (!textRecognizer.isOperational()) {
                    Toast.makeText(getApplicationContext(), "Detector dependecies are not yet available", Toast.LENGTH_SHORT).show();
                }
                else {
                    Frame frame = new Frame.Builder().setBitmap(selectedImage).build();
                    SparseArray<TextBlock> items = textRecognizer.detect(frame);
                    StringBuilder sb = new StringBuilder();
                    for(int x = 0; x < items.size(); x++){
                        TextBlock item = items.valueAt(x);
                        sb.append(item.getValue());
                        sb.append("\n");
                    }
                    if (sb.toString().isEmpty()){
                        tv_settext.setText("no text");
                        Toast.makeText(this, "no text", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        String text = sb.toString();
                        if(text.contains("86")){
                            Toast.makeText(context, "" + text, Toast.LENGTH_SHORT).show();
                            name.setText("Kevin Comedia");
                            phonenumber.setText("0922222222");
                            location.setText("Binangonan Rizal");
                            notes.setText("Hit and run");
                        }
                        tv_settext.setText(text);
                    }
                }
                imageview.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
        else if(requestCode == 2 && resultCode == RESULT_OK){
            Bitmap mCapturedPhotoSelf = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/platenumber/" + mFilenamePhotoID);
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(Environment.getExternalStorageDirectory() + "/platenumber/" + mFilenamePhotoID);
            } catch (IOException e) {
                e.printStackTrace();
            }

            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);


            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(mCapturedPhotoSelf, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(mCapturedPhotoSelf, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(mCapturedPhotoSelf, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = mCapturedPhotoSelf;
            }
//
            Log.d("bitmap" , " " + mCapturedPhotoSelf);


            imageview.setImageBitmap(rotatedBitmap);

            TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
            if (!textRecognizer.isOperational()) {
                Toast.makeText(getApplicationContext(), "Detector dependecies are not yet available", Toast.LENGTH_SHORT).show();
            }
            else {
                Frame frame = new Frame.Builder().setBitmap(rotatedBitmap).build();
                SparseArray<TextBlock> items = textRecognizer.detect(frame);
                StringBuilder sb = new StringBuilder();
                for(int x = 0; x < items.size(); x++){
                    TextBlock item = items.valueAt(x);
                    sb.append(item.getValue());
                    sb.append("\n");
                }
                if (sb.toString().isEmpty()){
                    tv_settext.setText("no text");
                    Toast.makeText(this, "no text", Toast.LENGTH_SHORT).show();
                }
                else {

                    String text = sb.toString();
                    if(text.contains("86")){
                        Toast.makeText(context, "" + text, Toast.LENGTH_SHORT).show();
                        name.setText("Kevin Comedia");
                        phonenumber.setText("0922222222");
                        location.setText("Binangonan Rizal");
                        notes.setText("Hit and run");
                    }
                    tv_settext.setText(text);
                }
            }
            Log.d("bitmap" , " " + mCapturedPhotoSelf);

        }
        else {
            Toast.makeText(getApplicationContext(), "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
}
