package com.goldendevs.tugis.Activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.darwindeveloper.horizontalscrollmenulibrary.custom_views.HorizontalScrollMenuView;
import com.goldendevs.tugis.Adapter.Navigation_3_Adapter;
import com.goldendevs.tugis.Model.Nav3ModelClass;
import com.goldendevs.tugis.R;
import com.goldendevs.tugis.app.App;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;

public class Home extends Base implements NavigationView.OnNavigationItemSelectedListener,RecognitionListener {

   // private ActionBarDrawerToggle actionBarDrawerToggle;
    // private DrawerLayout drawer;
   private SpeechRecognizer speech = null;
    private Intent recognizerIntent;

    private String LOG_TAG = "VoiceRecognitionActivity";
    private ProgressBar progressBar;
    private ToggleButton BtnVoiceRecognition;
    DuoDrawerLayout drawerLayout;
    private TabLayout mTabLayout;
    private Toolbar toolbar;
    NavigationView navigationView;
    RecyclerView recycleview;
    Navigation_3_Adapter navigation_3_adapter;
    Context context = this;
    TextToSpeech t1;
    HorizontalScrollMenuView menu;
    String voicetText = "";
    TextView btnMvHistory, btnMvReport;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Init();
        initMenu();
        SpeechTrigger();
        BtnVoiceRecognition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    sendNotification();
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    speech.startListening(recognizerIntent);
                } else {
                    progressBar.setIndeterminate(false);
                    progressBar.setVisibility(View.INVISIBLE);
                    speech.stopListening();
                }
            }
        });

        drawerLayout = (DuoDrawerLayout) findViewById(R.id.drawer_layout);
        DuoDrawerToggle drawerToggle = new DuoDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        drawerLayout.closeDrawer();

        setToolbar();



        invalidateOptionsMenu();

        Nav3ModelClass nav_3_modelClasses[] = {

                new Nav3ModelClass(R.drawable.ic_account_circle_black_24dp, "PROFILE", ""),
                new Nav3ModelClass(R.drawable.ic_home_black_24dp, "ACTIVITY", ""),
                new Nav3ModelClass(R.drawable.ic_fingerprint_white_24dp, "DRUG TEST REQUEST", ""),
                new Nav3ModelClass(R.drawable.ic_assignment_black_24dp, "TASK", "37"),
                new Nav3ModelClass(R.drawable.ic_assignment_black_24dp, "CRIME REPORT", "37"),
                new Nav3ModelClass(R.drawable.ic_assignment_black_24dp, "HISTORY", "10"),
                new Nav3ModelClass(R.drawable.ic_exit_to_app_black_24dp, "LOGOUT", "")


        };


        navigation_3_adapter = new Navigation_3_Adapter(Home.this, nav_3_modelClasses);
        recycleview.setLayoutManager(new LinearLayoutManager(Home.this));
        recycleview.setAdapter(navigation_3_adapter);


        menu.setOnHSMenuClickListener(new HorizontalScrollMenuView.OnHSMenuClickListener() {
            @Override
            public void onHSMClick(com.darwindeveloper.horizontalscrollmenulibrary.extras.MenuItem menuItem, int position) {
                if(menuItem.getText().toString().equals("Profile")){
                    startActivity(new Intent(context, Profile.class));

                }
                else if (menuItem.getText().toString().equals("Activity"))
                {
                    startActivity(new Intent(context, Home.class));
                }
                else if (menuItem.getText().toString().equals("DRUG TEST REQUEST"))
                {
                    startActivity(new Intent(context, FingerPrint.class));
                }
                else if (menuItem.getText().toString().equals("Report")) {
                    startActivity(new Intent(context, Report.class));

                }
                else if (menuItem.getText().toString().equals("History")) {

                }

            }
        });

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    Locale locale = new Locale("tl_PH","Philippines");
                    Locale.setDefault(locale);
                    t1.setLanguage(locale);
                }
            }
        });

        BtnVoiceRecognition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    speech.startListening(recognizerIntent);
                } else {
                    progressBar.setIndeterminate(false);
                    progressBar.setVisibility(View.INVISIBLE);
                    speech.stopListening();
                }
            }
        });

        btnMvHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,MVhistory.class));
            }
        });

        btnMvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MVReport.class));
            }
        });
    }

    private void Init(){
        menu = (HorizontalScrollMenuView)findViewById(R.id.img_list);
        BtnVoiceRecognition = (ToggleButton)findViewById(R.id.toggleButton1);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        btnMvHistory = (TextView)findViewById(R.id.mv_history);
        btnMvReport = (TextView)findViewById(R.id.mvreport);
    }

    private void initMenu() {
        menu.addItem("Profile", R.drawable.account90);
        menu.addItem("Activity", R.drawable.home90);
        menu.addItem("Report", R.drawable.assignment90);
        menu.addItem("History", R.drawable.assignment90);

    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {

            drawerLayout.closeDrawer(Gravity.LEFT); //OPEN Nav Drawer!
        } else {
            finish();
        }

    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle("");

        toolbar.findViewById(R.id.navigation_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawerLayout.openDrawer();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");
        progressBar.setIndeterminate(false);
        progressBar.setMax(3);
    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(LOG_TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(LOG_TAG, "onEndOfSpeech");
        progressBar.setIndeterminate(true);
        BtnVoiceRecognition.setChecked(false);
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(LOG_TAG, "FAILED " + errorMessage);
        Toast.makeText(context, "" + errorMessage.toString(), Toast.LENGTH_SHORT).show();
        BtnVoiceRecognition.setChecked(false);
    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {

        Log.i(LOG_TAG, "onEvent");
    }

    @Override
    public void onPartialResults(Bundle arg0) {

        Log.i(LOG_TAG, "onPartialResults");
    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(LOG_TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text += result + "\n";


        voicetText = matches.get(0).toString();
        Toast.makeText(context, "" +  matches.get(0), Toast.LENGTH_SHORT).show();
        if(voicetText.contains("tugis")){
            String response = "Kamusta Sayo. Anong maitutulong ko";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
        }

        if(voicetText.contains("profile")){
            String response = "Ang application ay papunta na sa profile";
            startActivity(new Intent(context, Profile.class));
        }

        if(voicetText.contains("report")){
            String response = "Ang application ay papunta na sa report";
            startActivity(new Intent(context, Report.class));
        }

        if(voicetText.contains("mobile verification")){
            String response = "Ang application ay papunta na sa report";
            startActivity(new Intent(context, MVReport.class));
        }

        if(voicetText.contains("drug test")){
            String response = "Ang application ay papunta na sa report";
            startActivity(new Intent(context, FingerPrint.class));
        }

        if(voicetText.contains("drug history")){
            String response = "Ang application ay papunta na sa drug history";
            startActivity(new Intent(context, DrugTestHistory.class));
        }


        if(voicetText.contains("emergency")){
            String response = "Darating na iyong kasamahan. Mag antay lang saglit";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
            sendNotification();
            SaveRecords();

        }

    }

    public void sendNotification() {

        //Get an instance of NotificationManager//

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle("Tugis")
                        .setContentText("We Help you as soon as possible!");


        // Gets an instance of the NotificationManager service//

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // When you issue multiple notifications about the same type of event,
        // it’s best practice for your app to try to update an existing notification
        // with this new information, rather than immediately creating a new notification.
        // If you want to update this notification at a later date, you need to assign it an ID.
        // You can then use this ID whenever you issue a subsequent notification.
        // If the previous notification is still visible, the system will update this existing notification,
        // rather than create a new one. In this example, the notification’s ID is 001//

        mNotificationManager.notify(001, mBuilder.build());
    }

    public void SaveRecords() {
        Log.d("test1","test");
        String getDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://"+getApiUrl()+""+ENDPOINT+"saveEmergency", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("test1","test" + response);
//                    Toast.makeText(context, "response", Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(response);
                    if(obj.getString("error").equals("false")) {
                        Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(context, "Please check username and passw", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Iwatcher", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("name", "jeffrey valedo");
                params.put("location", "PNP ITMS");

                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }



















    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null) {
            speech.destroy();
            Log.i(LOG_TAG, "destroy");
        }

        if(t1 != null){

            t1.stop();
            t1.shutdown();
        }
        super.onPause();

    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    //
    private void SpeechTrigger(){
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,"fil_PH");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
    }
}
