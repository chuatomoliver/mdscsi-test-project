package com.goldendevs.tugis.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goldendevs.tugis.Adapter.ProfileAdapter;
import com.goldendevs.tugis.Model.ProfileModel;
import com.goldendevs.tugis.R;

import java.util.ArrayList;

public class JobDone extends Fragment {
    Integer[] bookimg = {R.drawable.ic_done_gold_24dp,R.drawable.ic_done_gold_24dp,R.drawable.ic_done_gold_24dp,R.drawable.ic_done_gold_24dp,R.drawable.ic_done_gold_24dp,R.drawable.ic_done_gold_24dp};

    String[] booktext = {"Manila Mayor Isko Moreno forms his own mayoral anti-crime police squad, headed by a cop who grew up with him in Tondo",
            "Police say two unidentified suspects fled the scene of the crime aboard a pink Yamaha Mio with no plate number",
            "The Anti-Crime Council of the Philippines is expected to focus on bringing down smuggling in the country",
            "The Baguio City Hall façade has been turned orange since November 25 to support the '18 Days to End Violence Against Women' campaign\n",
            "Police say Randy Oavenada used shabu, and drug paraphernalia were also found at the crime scene",
            "From 2,350 robberies in January 2016, the Philippine National Police records only 1,295 cases in October 2017 – a sharp 45% dip\n"};
    String[] booktext1 = {"How Isko Moreno copied a Duterte anti-crime idea and made it his own",
            "DENR employee shot dead by riding-in-tandem suspects in Antipolo",
            "Bato dela Rosa is president of newly formed PH Anti-Crime Council",
            "Baguio: Rape cases up, minors involved in street crimes",
            "Suspect in rape-slay of Mabel Cama tests positive for drugs",
            "Robberies at all-time low in 22 months – PNP"};


    private ProfileAdapter homepageAdapter;
    private RecyclerView recyclerview;
    private ArrayList<ProfileModel> bookmarksModelArrayList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.profilerecyclerview,container,false);


        recyclerview = view.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager( getContext(),1);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());

        bookmarksModelArrayList = new ArrayList<>();


        for (int i = 0; i < bookimg.length; i++) {
            ProfileModel view1 = new ProfileModel( bookimg[i],booktext[i],booktext1[i]);
            bookmarksModelArrayList.add(view1);
        }
        homepageAdapter = new ProfileAdapter(getActivity(),bookmarksModelArrayList);
        recyclerview.setAdapter(homepageAdapter);

        return view;
    }
}
