package com.goldendevs.mcdo.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.goldendevs.mcdo.Activity.Cart;
import com.goldendevs.mcdo.Adapter.MenuOrderAdapter;
import com.goldendevs.mcdo.Models.BookOrder;
import com.goldendevs.mcdo.R;

import java.util.ArrayList;


public class FirstFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
     int mPageNo;

    TextView title;
    private ArrayList<BookOrder> bookOrderModelClasses;
    private RecyclerView recyclerView;
     MenuOrderAdapter menuOrderAdapter;

    Context context;
    private Integer image[] = {R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1};
    private String txt[]={"Scan Barcode","Profile","Buy","Bid","Payment","Rate Crops","Rate Crops","Rate Crops"};
    private ImageView cart;

    public FirstFragment() {
        // Required empty public constructor
    }

    public static FirstFragment newInstance(int pageNo) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);


        title = view.findViewById(R.id.toolbar_text);

        title.setText("Store");

        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        bookOrderModelClasses = new ArrayList<>();

        for (int i = 0; i < image.length; i++) {
            BookOrder beanClassForRecyclerView_contacts = new BookOrder(image[i],txt[i]);
            bookOrderModelClasses.add(beanClassForRecyclerView_contacts);
        }
        menuOrderAdapter = new MenuOrderAdapter(context,bookOrderModelClasses);
        recyclerView.setAdapter(menuOrderAdapter);
        cart = view.findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Cart.class));
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
