package com.goldendevs.mcdo.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.goldendevs.mcdo.Fragments.BurgerFragment;
import com.goldendevs.mcdo.Fragments.FirstFragment;
import com.goldendevs.mcdo.Fragments.FourthFragment;
import com.goldendevs.mcdo.Fragments.SecondFragment;
import com.goldendevs.mcdo.R;

public class ViewPageAdapter extends FragmentPagerAdapter {
    private Context context;

    private final String[] mTabsTitle = {"Home", "Menu", "Store locator","Coupon"};
    private int mTabsIcons[];


    public ViewPageAdapter(FragmentManager fm,Context context, int images[]) {
        super(fm);
        this.context = context;
        this.mTabsIcons = images;
    }

    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        ImageView icon = view.findViewById(R.id.icon);
        TextView title = view.findViewById(R.id.title);
        icon.setImageResource(mTabsIcons[position]);
        title.setText(mTabsTitle[position]);
        Log.d("fetchi",mTabsTitle[position].toString());
        return view;
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {

            case 0:
                return FirstFragment.newInstance(1);

            case 1:
                return SecondFragment.newInstance(2);
            case 2:
                return BurgerFragment.newInstance(3);
            case 3:
                return FourthFragment.newInstance(4);
        }
        return null;
    }

    @Override
    public int getCount() {
        int PAGE_COUNT = 4;
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabsTitle[position];
    }
}
