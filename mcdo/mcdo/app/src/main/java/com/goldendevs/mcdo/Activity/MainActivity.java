package com.goldendevs.mcdo.Activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.goldendevs.mcdo.Adapter.ViewPageAdapter;
import com.goldendevs.mcdo.R;

public class MainActivity extends AppCompatActivity {

     TabLayout mTabLayout;

    private int[] mTabsIcons = {
            R.drawable.ic_mcdo,
            R.drawable.ic_burger,
            R.drawable.ic_pin,
            R.drawable.ic_coupon};

    private String txt[]={"Home","Menu","Store Locator","Coupon"};


    Context context = this;
    ViewPageAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup the viewPager
        ViewPager viewPager = findViewById(R.id.view_pager);
         pagerAdapter = new ViewPageAdapter(getSupportFragmentManager(),context, mTabsIcons);
        if (viewPager != null)
            viewPager.setAdapter(pagerAdapter);

        mTabLayout = findViewById(R.id.tab_layout);
        if (mTabLayout != null) {
            mTabLayout.setupWithViewPager(viewPager);

            for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }


            mTabLayout.getTabAt(0).getCustomView().setSelected(true);

        }
    }
}
