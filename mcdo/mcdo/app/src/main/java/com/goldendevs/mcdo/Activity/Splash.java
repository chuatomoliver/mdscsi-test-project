package com.goldendevs.mcdo.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.goldendevs.mcdo.R;

public class Splash extends AppCompatActivity {

    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(context,Signin.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                }
            }
        }, 3000);
    }
}
