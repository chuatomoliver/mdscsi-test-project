package com.goldendevs.mvpboilerplate.ui.Login.presenter;



import com.goldendevs.mvpboilerplate.model.Posts;
import com.goldendevs.mvpboilerplate.service.MyApi;
import com.goldendevs.mvpboilerplate.service.RetrofitClient;
import com.goldendevs.mvpboilerplate.ui.Login.MvpView.PostView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class PostPresenter {
    private PostView postView;
    // Retrofit use for Api service
    private RetrofitClient retrofitClient;
    // CompositeDispoable use for Rxjava
    // Rxjava use for background threading
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    MyApi myAPI;

    public PostPresenter(PostView view){
        this.postView = view;

        if(this.retrofitClient == null){
            this.retrofitClient = new RetrofitClient();
        }

        Retrofit retrofit = RetrofitClient.getInstance();
        myAPI = retrofit.create(MyApi.class);
    }

    public void getPost() {
        compositeDisposable.add(myAPI.getPost()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Posts>>() {
                    @Override
                    public void accept(List<Posts> posts) throws Exception {
                        postView.onListPost(posts);
                    }
                }));

    }
}
