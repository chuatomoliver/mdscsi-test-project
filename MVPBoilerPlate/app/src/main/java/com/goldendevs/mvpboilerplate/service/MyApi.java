package com.goldendevs.mvpboilerplate.service;

import com.goldendevs.mvpboilerplate.model.Posts;



import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface MyApi {
    // end point of url
    // e.g https://jsonplaceholder.typicode.com/posts.
    // check on postman to view the json response
    @GET("posts")
    Observable<List<Posts>> getPost();
}
