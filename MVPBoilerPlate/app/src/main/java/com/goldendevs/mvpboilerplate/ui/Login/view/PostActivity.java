package com.goldendevs.mvpboilerplate.ui.Login.view;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.goldendevs.mvpboilerplate.R;
import com.goldendevs.mvpboilerplate.model.Posts;
import com.goldendevs.mvpboilerplate.ui.Login.MvpView.PostView;
import com.goldendevs.mvpboilerplate.ui.Login.presenter.PostPresenter;

import java.util.ArrayList;
import java.util.List;

public class PostActivity extends AppCompatActivity implements PostView {

    List<Posts> listPost = new ArrayList<>();
//    CompositeDisposable compositeDisposable = new CompositeDisposable();

//    MyApi myAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Trigger to call presenter
        PostPresenter postPresenter = new PostPresenter(this);
        // trigger to call getPost
        postPresenter.getPost();


    }

    // Interface for mvpView
    @Override
    public void onListPost(List<Posts> posts) {
        for (int x = 0; x < 10; x++){
            Log.d("result1234", "" + posts.get(x).getTitle());
        }

    }







}
