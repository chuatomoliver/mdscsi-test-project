package com.goldendevs.mvpboilerplate.ui.Login.MvpView;

import com.goldendevs.mvpboilerplate.model.Posts;

import java.util.List;

/**
 * Created by bpn on 11/30/17.
 */

public interface PostView {
    // Medium for MvpView
    void onListPost(List<Posts> posts);
}
