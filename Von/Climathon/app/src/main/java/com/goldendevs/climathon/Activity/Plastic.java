package com.goldendevs.climathon.Activity;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.goldendevs.climathon.Adapter.ProductAdapter;
import com.goldendevs.climathon.Models.ProductObject;
import com.goldendevs.climathon.R;

import java.util.ArrayList;

public class Plastic extends AppCompatActivity {

    String[] metalname = {"Plastic cups(clear/transparent)", "Plastic(ordinary - basins, containers, etc)", "Plastic Bottles(softdrinks, juices, water,etc)"};
    String[] price = {"\u20B1 12.25/kg","\u20B1 14.00/kg","\u20B1 0.50/kg"};
    String[] percent = {"22.10%","-7.5%","12.40%"};

    private ListView lv_items;
    private ArrayList<ProductObject> list_products = new ArrayList<>();
    private ProductAdapter productAdapter;
    private Context context = this;
    private TextView tv_title;
    private String title;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plastic);
        InitVariables();

        for(int x = 0; x < metalname.length; x ++){
            ProductObject productObject = new ProductObject();

            Log.d("sizes123", price.length+" " + metalname.length +"" + percent.length);
            productObject.setName(metalname[x]);
            productObject.setWeight(price[x]);
            productObject.setPercent(percent[x]);
            list_products.add(productObject);
        }

        productAdapter = new ProductAdapter(list_products,context );
        lv_items.setAdapter(productAdapter);

        tv_title.setText("Plastic");
        toolbar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(context, R.color.pureyellow)));
    }

    private void InitVariables(){
        lv_items = (ListView)findViewById(R.id.lv_items);
        tv_title = findViewById(R.id.toolbar_text);
        toolbar = findViewById(R.id.toolbar);
    }
}
