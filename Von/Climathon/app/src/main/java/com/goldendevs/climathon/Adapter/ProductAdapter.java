package com.goldendevs.climathon.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.goldendevs.climathon.Models.ProductObject;
import com.goldendevs.climathon.R;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {

    ArrayList<ProductObject> list_product = new ArrayList<>();
    Context context;

    public ProductAdapter(ArrayList<ProductObject> list_product, Context context) {
        this.list_product = list_product;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list_product.size();
    }

    @Override
    public Object getItem(int position) {
        return list_product.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.list_view_product_item, parent, false);
        TextView tv_name = (TextView)convertView.findViewById(R.id.tv_product_name);
        TextView tv_price = (TextView)convertView.findViewById(R.id.tv_kilo);
        TextView tv_percent = (TextView)convertView.findViewById(R.id.tv_percent);

        tv_name.setText(list_product.get(position).getName());
        tv_percent.setText(list_product.get(position).getPercent());
        tv_price.setText(list_product.get(position).getWeight());

        return convertView;
    }
}
