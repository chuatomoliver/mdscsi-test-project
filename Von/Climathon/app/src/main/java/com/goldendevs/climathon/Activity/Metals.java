package com.goldendevs.climathon.Activity;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.goldendevs.climathon.Adapter.ProductAdapter;
import com.goldendevs.climathon.Models.ProductObject;
import com.goldendevs.climathon.R;

import java.util.ArrayList;

public class Metals extends AppCompatActivity {

    String[] metalname = {"Cooper red", "Cooper yellow", "Aluminum", "Aluminum(jalousey franing)"," aluminum cans","Aluminum bottle caps","Zinc","Lead","Stainless"};
    String[] price = {"\u20B1 150.25/kg","\u20B1 105.00/kg","\u20B1 40.00/kg","\u20B1 50.25kg","\u20B1 35.00/kg","\u20B1 20.00","\u20B1 20.25/kg","\u20B1 35.00/kg","\u20B1 45.00/kg"};
    String[] percent = {"25.20%","-7.5%","12.40%","12.50%","-15.5%","12.40%","25.0%","-10.0%","12.40%"};

    private ListView lv_items;
    private ArrayList<ProductObject> list_products = new ArrayList<>();
    private Context context = this;
    private TextView tv_title;
    private String title;
    private Toolbar toolbar;
    ProductAdapter productAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metals);
        InitVariables();

        for(int x = 0; x < metalname.length; x ++){
            ProductObject productObject = new ProductObject();

            Log.d("sizes123", price.length+" " + metalname.length +"" + percent.length);
            productObject.setName(metalname[x]);
            productObject.setWeight(price[x]);
            productObject.setPercent(percent[x]);
            list_products.add(productObject);
        }

        productAdapter = new ProductAdapter(list_products,context );
        lv_items.setAdapter(productAdapter);

        tv_title.setText("Metals");
        toolbar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(context, R.color.purered)));

    }

    private void InitVariables(){
        lv_items = findViewById(R.id.lv_items);
        tv_title = findViewById(R.id.toolbar_text);
        toolbar = findViewById(R.id.toolbar);
    }
}
