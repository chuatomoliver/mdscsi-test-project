package com.goldendevs.climathon.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.goldendevs.climathon.Activity.Glass;
import com.goldendevs.climathon.Activity.Metals;
import com.goldendevs.climathon.Activity.Plastic;
import com.goldendevs.climathon.R;

public class FirstFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;

    RelativeLayout LayoutMetals;
    RelativeLayout LayoutGlass;
    RelativeLayout LayoutBottle;
    LinearLayout LayoutSelection1;
    LinearLayout LayoutSelection2;
    CalendarView calendarView;

    public FirstFragment() {
        // Required empty public constructor
    }

    public static FirstFragment newInstance(int pageNo) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        LayoutMetals = view.findViewById(R.id.layout_metals);
        LayoutGlass = view.findViewById(R.id.glass);
        LayoutBottle = view.findViewById(R.id.bottle);
        calendarView = view.findViewById(R.id.calendarView);

        LayoutSelection1 = view.findViewById(R.id.layout_trash1);
        LayoutSelection2 = view.findViewById(R.id.layout_trash2);

        LayoutMetals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Metals.class));
            }
        });

        LayoutGlass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Glass.class));
            }
        });

        LayoutBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Plastic.class));
            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                if((dayOfMonth%2) == 0) {
                    LayoutSelection1.setVisibility(View.GONE);
                    LayoutSelection2.setVisibility(View.VISIBLE);
                }
                else{
                    LayoutSelection1.setVisibility(View.VISIBLE);
                    LayoutSelection2.setVisibility(View.GONE);
                }



            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
