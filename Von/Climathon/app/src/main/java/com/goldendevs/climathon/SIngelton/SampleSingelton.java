package com.goldendevs.climathon.SIngelton;

public class SampleSingelton {
    String editValue;

    public static final SampleSingelton ourInstance = new SampleSingelton();

    public static SampleSingelton getInstance(){
        return ourInstance;
    }


    private SampleSingelton(){ }
    public void setText(String editValue){
        this.editValue = editValue;
    }

    public String getText(){
        return editValue;
    }
}
