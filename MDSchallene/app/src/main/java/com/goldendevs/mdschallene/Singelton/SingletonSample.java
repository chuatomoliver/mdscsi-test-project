package com.goldendevs.mdschallene.Singelton;


import com.goldendevs.mdschallene.Models.FoodMenu;
import com.goldendevs.mdschallene.Models.Order;
import com.goldendevs.mdschallene.R;

import java.util.ArrayList;

public class SingletonSample {

    private static SingletonSample instance;

    public static SingletonSample GetInstance(){
        if (instance == null){
            instance = new SingletonSample();
        }
        return  instance;
    }


    private ArrayList<FoodMenu> foodMenuList;

    public ArrayList<FoodMenu> getFoodMenuList() {
        return foodMenuList;
    }

    private FoodMenu currentItem;

    public FoodMenu getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(FoodMenu currentItem) {
        this.currentItem = currentItem;
    }

    private Order currentOrder;


    public void InitMenu(){
        if (foodMenuList == null)
            foodMenuList = new ArrayList<FoodMenu>();
        foodMenuList.add(new FoodMenu("Sample Food 1","This is sample food 1", R.drawable.menu1, 105));
        foodMenuList.add(new FoodMenu("Sample Food 2","This is sample food 2", R.drawable.menu1, 130));
        foodMenuList.add(new FoodMenu("Sample Food 3","This is sample food 3", R.drawable.menu1, 65));
        foodMenuList.add(new FoodMenu("Sample Food 4","This is sample food 4", R.drawable.menu1,80));
        foodMenuList.add(new FoodMenu("Sample Food 5","This is sample food 5", R.drawable.menu1,190));
        foodMenuList.add(new FoodMenu("Sample Food 6","This is sample food 6", R.drawable.menu1,40));
    }


    public Order getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Order currentOrder) {
        this.currentOrder = currentOrder;
    }
}
