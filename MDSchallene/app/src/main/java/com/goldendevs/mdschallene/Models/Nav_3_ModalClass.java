package com.goldendevs.mdschallene.Models;

/**
 * Created by Remmss on 04-10-17.
 */

public class Nav_3_ModalClass {

    int image;
    String name;
    String count;


    public Nav_3_ModalClass(int image, String name, String count) {
        this.image = image;
        this.name = name;
        this.count = count;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
