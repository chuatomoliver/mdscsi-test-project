package com.goldendevs.mdschallene.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.goldendevs.mdschallene.Adapter.BookOrderAdapter;
import com.goldendevs.mdschallene.Models.BookOrder;
import com.goldendevs.mdschallene.R;

import java.util.ArrayList;

public class FirstFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    int mPageNo;

    private ArrayList<BookOrder> bookOrderModelClasses = new ArrayList<>();
    private RecyclerView recyclerView;
    private BookOrderAdapter bAdapter;

    private Integer image[] =
            {R.drawable.ic_communication,
                    R.drawable.ic_communication,
                    R.drawable.ic_communication,
                    R.drawable.ic_communication,
                    R.drawable.ic_communication,
                    R.drawable.ic_communication};

    private String txt[]={"Scan Barcode","Profile","Image Process","Transactions","Payment","Monitoring"};

    public FirstFragment() {
        // Required empty public constructor
    }

    public static FirstFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        for (int i = 0; i < image.length; i++) {
            BookOrder beanClassForRecyclerView_contacts = new BookOrder(image[i],txt[i]);
            bookOrderModelClasses.add(beanClassForRecyclerView_contacts);
        }
        bAdapter = new BookOrderAdapter(getActivity(),bookOrderModelClasses);
        recyclerView.setAdapter(bAdapter);
        return view;
    }

}
