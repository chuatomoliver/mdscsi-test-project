package com.goldendevs.mdschallene.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.goldendevs.mdschallene.Adapter.ProfileAdapter;
import com.goldendevs.mdschallene.Models.ProfileModel;
import com.goldendevs.mdschallene.R;

import java.util.ArrayList;


public class Credentials extends Fragment {
    Integer[] bookimg = {R.drawable.ic_aim,R.drawable.ic_ateneo,R.drawable.ic_de_lasalle,R.drawable.nbcnightly,R.drawable.rbcimg,R.drawable.news1};

    String[] booktext = {"Graduated Master's in Information Management Technology",
            "Graduated Bachelor of Science in Information Technology",
            "Graduate Bachelor of Science in Medical Technology major in Physical Education",
            "How to Plan\nYour First Ski \nVacation",
            "How Social \nIsolation Is\nKilling Us",
            "Use Labels to\nSort Messages\nin Facebook"};
    String[] booktext1 = {"Asian Institue of Management","ATENEO DE MANILA","DE LA SALLE","ROTC","PNP","UFC CHAMPION"};


    private ProfileAdapter homepageAdapter;
    private RecyclerView recyclerview;
    private ArrayList<ProfileModel> bookmarksModelArrayList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.profilerecyclerview,container,false);


        recyclerview = view.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager( getActivity(),2);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());

        bookmarksModelArrayList = new ArrayList<>();


        for (int i = 0; i < bookimg.length; i++) {
            ProfileModel view1 = new ProfileModel( bookimg[i],booktext[i],booktext1[i]);
            bookmarksModelArrayList.add(view1);
        }
        homepageAdapter = new ProfileAdapter(getActivity(),bookmarksModelArrayList);
        recyclerview.setAdapter(homepageAdapter);

        return view;
    }
}
