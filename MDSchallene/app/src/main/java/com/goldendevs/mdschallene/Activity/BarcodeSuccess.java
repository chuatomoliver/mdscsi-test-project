package com.goldendevs.mdschallene.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.goldendevs.mdschallene.Adapter.RecyclerAdapter;
import com.goldendevs.mdschallene.Models.ModelClass;
import com.goldendevs.mdschallene.R;
import com.goldendevs.mdschallene.SingeltonOne;
import com.goldendevs.mdschallene.app.App;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BarcodeSuccess extends AppCompatActivity {

    private ArrayList<ModelClass> homeListModelClassArrayList;
    private RecyclerView recyclerView;
    private RecyclerAdapter mAdapter;
    Context context = this;
    Button btnTransaction;
    Button btnBunds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_success);

        String iteamName[]={"Event #1"};
        String quantity[]={"1"};
        String price[]={"100,000"};
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        homeListModelClassArrayList = new ArrayList<>();
        for (int i = 0; i < iteamName.length; i++) {
            ModelClass beanClassForRecyclerView_contacts = new ModelClass(iteamName[i],quantity[i],price[i]);
            homeListModelClassArrayList.add(beanClassForRecyclerView_contacts);
        }
        mAdapter = new RecyclerAdapter(context,homeListModelClassArrayList);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        btnTransaction = (Button)findViewById(R.id.btnBunds);
        btnTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGeneratePGI();
//                SaveRecords();
            }
        });
    }

    public void getGeneratePGI() {

        StringRequest postRequest = new StringRequest(Request.Method.POST, "https://pgi-ws-staging.bayadcenter.net/api/v1/transactions/generate", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject obj = new JSONObject(response);
                    String myString = obj.getJSONObject("data").getString("url");

                    Intent intent = new Intent(context, Webview.class);
                    intent.putExtra("url", myString);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Iwatcher", e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {

            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("amount", "1");
                params.put("txnid", "1111");
                params.put("digest", "fea3908891c9b86d9cb5f27cfc344d62d1359ff8");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("X-BayadCenter-Token","2c1816316e65dbfcb0c34a25f3d6fe5589aef65d");
                params.put("X-BayadCenter-Code","MSYS_TEST_BILLER");
                return params;
            }

        };


        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }

    public void SaveRecords() {
        String getDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        StringRequest postRequest = new StringRequest(Request.Method.POST,  "http://192.168.1.240/PNP/api/v1/index.php/saveReport", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    Toast.makeText(context, "response", Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(response);
                    if(obj.getString("error").equals("false")) {
                        Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(context, "Please check username and passw", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Iwatcher", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("path", "1");
                params.put("scenario", "1");
                params.put("name", "1");
                params.put("phone", "1");
                params.put("location", "1");
                params.put("notes", "1");
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }
}
