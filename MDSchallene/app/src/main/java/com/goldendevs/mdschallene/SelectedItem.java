package com.goldendevs.mdschallene;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.goldendevs.mdschallene.Models.FoodMenu;
import com.goldendevs.mdschallene.Models.Order;
import com.goldendevs.mdschallene.Singelton.SingletonSample;

public class SelectedItem extends AppCompatActivity {

    Context context = this;
    TextView tv_count1, tv_count2,tv_count3;
    int count1 = 1, count2 = 1, count3 = 1;
    FoodMenu mFoodMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_item);
        mFoodMenu = SingletonSample.GetInstance().getCurrentItem();

        InitElements();


    }

    private void InitElements() {
        tv_count1 = findViewById(R.id.tv_s_count1);
        tv_count2 = findViewById(R.id.tv_s_count2);
        tv_count3 = findViewById(R.id.tv_s_count3);

        TextView tv_title = findViewById(R.id.tv_s_title);
        ImageView img_pic = findViewById(R.id.iv_s_pic);

//        tv_title.setText(mFoodMenu.getName());
//        Glide.with(this).load(mFoodMenu.getThumbnail()).into(img_pic);

        Button btn_f_minus = findViewById(R.id.btn_s_first_minus);
        Button btn_f_plus = findViewById(R.id.btn_s_first_plus);
        btn_f_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMinus(1, false);
                tv_count1.setText(count1 + "");
            }
        });


        btn_f_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMinus(1, true);
                tv_count1.setText(count1 + "");
            }
        });

        Button btn_s_minus = findViewById(R.id.btn_s_second_minus);
        Button btn_s_plus = findViewById(R.id.btn_s_second_plus);
        btn_s_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMinus(2, false);
                tv_count2.setText(count2 + "");
            }
        });

        btn_s_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMinus(2, true);
                tv_count2.setText(count2 + "");
            }
        });

        Button btn_t_minus = findViewById(R.id.btn_s_third_minus);
        Button btn_t_plus = findViewById(R.id.btn_s_third_plus);
        btn_t_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMinus(3, false);
                tv_count3.setText(count3 + "");
            }
        });

        btn_t_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMinus(3, true);
                tv_count3.setText(count3 + "");
            }
        });

        Button btn_place_order = findViewById(R.id.btn_place_order);
        btn_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order order = new Order(mFoodMenu, count1, count2, count3);
                SingletonSample.GetInstance().setCurrentOrder(order);
                //Toast.makeText(v.getContext(), order.getFoodMenu().getName() + " " + order.getFoodCount() + " drinkCount " + order.getDrinkCount(), Toast.LENGTH_SHORT).show();
                context.startActivity(new Intent(context, Cart.class));
            }
        });
    }

    private int AddMinus(int value, boolean bool) {

        switch (value)
        {
            case 1:
                if (bool)
                {
                    if (count1 >= 1)
                        return  count1 = count1 + 1;
                    else
                        return  1;
                }
                else {
                    if (count1 <= 1)
                        return  1;
                    else
                        return  count1 = count1 - 1;
                }
            case 2:
                if (bool)
                {
                    if (count2 >= 1)
                        return  count2 = count2 + 1;
                    else
                        return  1;
                }
                else {
                    if (count2 <= 1)
                        return  1;
                    else
                        return  count2 = count2 - 1;
                }
            case 3:
                if (bool)
                {
                    if (count3 >= 1)
                        return  count3 = count3 + 1;
                    else
                        return  1;
                }
                else {
                    if (count3 <= 1)
                        return  1;
                    else
                        return  count3  = count3 - 1;
                }
            default:
                return 0;
        }
    }
}
