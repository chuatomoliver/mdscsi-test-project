package com.goldendevs.mdschallene.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.goldendevs.mdschallene.R;
import com.goldendevs.mdschallene.SingeltonOne;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONObject;

import java.util.Arrays;

import customfonts.MyTextView_Roboto_Regular;
import de.hdodenhof.circleimageview.CircleImageView;

public class SignIn extends Base {

    MyTextView_Roboto_Regular btnSign;
    ImageView logo;

    private LoginButton loginButton;
    private CircleImageView circleImageView;
    private TextView txtName,txtEmail;
    private FirebaseAuth mAuth;

    private CallbackManager callbackManager;
    String first_name;
    String last_name;
    String email;
    String id;
    String image_url;

    // for google auth
    static final int GOOGLE_SIGN_IN = 123;
    Button btn_login, btn_logout;
    TextView text;
    ImageView image;
    String name;
    ProgressBar progressBar;
    GoogleSignInClient mGoogleSignInClient;
    Context context = this;

    int status = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        InitVariables();
        btnSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
                finish();
            }
        });

        logo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("API URL");
                builder.setMessage("Enter API URL: 192.168.1.2:80");

                final EditText input = new EditText(v.getContext());
                input.setText(getApiUrl());
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setMaxLines(1);

                builder.setView(input);

                builder.setNeutralButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String apiUrl = input.getText().toString();
                        SharedPreferences sharedPref = getSharedPreferences("API_URL", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("API_URL", apiUrl);
                        editor.commit();
                    }
                });

                builder.show();
                return false;
            }
        });


        google();
        loginButton.setReadPermissions(Arrays.asList("email","public_profile"));
        checkLoginStatus();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(context ,"Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(context ,"Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(context ,"Success", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void InitVariables(){
        btnSign = findViewById(R.id.btnSignIn);
        logo = findViewById(R.id.logo);

        loginButton = findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);
        mAuth = FirebaseAuth.getInstance();

        btn_login = findViewById(R.id.btn_login);

        progressBar = findViewById(R.id.progress_bar_circular);
    }

    private void google(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);

        btn_login.setOnClickListener(v -> SignInGoogle());
//        btn_logout.setOnClickListener(v -> Logout());

        if (mAuth.getCurrentUser() != null) {
            FirebaseUser user = mAuth.getCurrentUser();
            updateUI(user);
        }
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            String name1 = user.getDisplayName();
            String email1 = user.getEmail();
            String photo = String.valueOf(user.getPhotoUrl());

            name = name1;
            email = email1;

        } else {
            text.setText("Firebase Login \n");
//            Picasso.with(context).load(R.drawable.ic_firebase_logo).into(image);
//            btn_logout.setVisibility(View.INVISIBLE);
            btn_login.setVisibility(View.VISIBLE);
        }
    }

    public void SignInGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }

    private void checkLoginStatus()
    {
        if(AccessToken.getCurrentAccessToken()!=null)
        {
            loaduserProfile(AccessToken.getCurrentAccessToken());
        }
    }

    private void loaduserProfile(AccessToken newAccessToken){
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    first_name = object.getString("first_name");
                    last_name = object.getString("last_name");
                    email = object.getString("email");
                    id = object.getString("id");
                    image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                    status = 1;
                    Log.d("results fb", "" + first_name + " " + last_name + " " + email + " " + id);
                    Login(status);

                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.dontAnimate();

//                    Glide.with(WelcomeActivity.this).load(image_url).into(circleImageView);

                }catch (Exception ex){
                    Log.d("Error facebook", "" + ex.getMessage());
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void Login(int status){
        if(status == 1){
            // facebook
            SingeltonOne.getInstance().name = first_name + " " + last_name;
            SingeltonOne.getInstance().email = email;
            SingeltonOne.getInstance().status = status;
            SingeltonOne.getInstance().img_url = image_url;
            startActivity(new Intent(context, WelcomeActivity.class));
            finish();
        }
        else if(status == 2){
            SingeltonOne.getInstance().name = name;
            SingeltonOne.getInstance().email = email;
            SingeltonOne.getInstance().status = status;
            startActivity(new Intent(context, WelcomeActivity.class));
            finish();
        }
    }
}
