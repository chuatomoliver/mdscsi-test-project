package com.goldendevs.mdschallene.Activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;


import com.goldendevs.mdschallene.Adapter.HomePageAdapter;
import com.goldendevs.mdschallene.R;

import me.relex.circleindicator.CircleIndicator;


public class HomePager extends AppCompatActivity {

    private ViewPager v1;
    private HomePageAdapter a;
    private CircleIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_pager);
        v1 = findViewById(R.id.v1);
         indicator = findViewById(R.id.indicator);
        a = new HomePageAdapter(getSupportFragmentManager());
        v1.setAdapter(a);
        indicator.setViewPager(v1);
        a.registerDataSetObserver(indicator.getDataSetObserver());
    }


}
