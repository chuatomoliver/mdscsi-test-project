package com.goldendevs.mdschallene.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.goldendevs.mdschallene.R;
import com.goldendevs.mdschallene.Scanner.IntentIntegrator;
import com.goldendevs.mdschallene.Scanner.IntentResult;


public class Scan extends AppCompatActivity {

    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        IntentIntegrator scanIntegrator = new IntentIntegrator(Scan.this);
        scanIntegrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            startActivity(new Intent(context, BarcodeSuccess.class));
            finish();
//            mInputBarcode.setText(scanContent);
//            getProductDetails(scanContent);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
        //calculate();
    }
}
