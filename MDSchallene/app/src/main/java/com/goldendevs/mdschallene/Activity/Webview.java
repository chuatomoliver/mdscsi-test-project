package com.goldendevs.mdschallene.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.goldendevs.mdschallene.MainActivity;
import com.goldendevs.mdschallene.R;
import com.goldendevs.mdschallene.SingeltonOne;
import com.goldendevs.mdschallene.app.App;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Webview extends Base {

    Context context = this;
    String txnid ="",callback = "",digets = "", ref= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        WebView webView = findViewById(R.id.webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.loadUrl(getIntent().getExtras().getString("url"));

        Uri uri = Uri.parse(getIntent().getExtras().getString("url"));
        String token = uri.getLastPathSegment();
        Log.d("token12","" + getLastBitFromUrl(token));

        if(token !=  null){
            getTransactions();
        }



    }

    public static String getLastBitFromUrl(final String url){
        // return url.replaceFirst("[^?]*/(.*?)(?:\\?.*)","$1);" <-- incorrect
        return url.replaceFirst(".*/([^/?]+).*", "$1");
    }

    public void getTransactions() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, "https://pgi-ws-staging.bayadcenter.net/api/v1/transactions", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject obj = new JSONObject(response);
                    ref  = obj.getJSONObject("data").getString("refno");
                    txnid  = obj.getJSONObject("data").getString("txnid");
                    digets  = obj.getJSONObject("data").getString("digest");



                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Iwatcher", e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {

            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("amount", "1");
                params.put("txnid", "1111");
                params.put("digest", "fea3908891c9b86d9cb5f27cfc344d62d1359ff8");
                params.put("callback_url", "MP4WIUFQZD");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("X-BayadCenter-Token","2c1816316e65dbfcb0c34a25f3d6fe5589aef65d");
                params.put("X-BayadCenter-Code","MSYS_TEST_BILLER");
                return params;
            }

        };


        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, MainActivity.class));
        finish();
    }
}
