package com.goldendevs.mdschallene.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.goldendevs.mdschallene.fragments.Credentials;
import com.goldendevs.mdschallene.fragments.JobDone;

public class TabProfile extends FragmentStatePagerAdapter {

    int numoftabs;
    public TabProfile(FragmentManager fm, int mnumoftabs) {
        super(fm);
        this.numoftabs = mnumoftabs;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Credentials tab1 = new Credentials();
                return tab1;

            case 1:
                JobDone tab2 = new JobDone();
                return tab2;

//            case 2:
//                Credentials tab3 = new Credentials();
//                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numoftabs;
    }
}
