package com.goldendevs.mdschallene.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.goldendevs.mdschallene.MainActivity;
import com.goldendevs.mdschallene.Models.RegisterObject;
import com.goldendevs.mdschallene.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Registration extends AppCompatActivity {

    EditText edt_email,edt_password;
    Button btnRegister;
    DatabaseReference databaseReference;
    Context context = this;
    LinearLayout register;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Init();
        databaseReference = FirebaseDatabase.getInstance().getReference("Account");
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register();
            }
        });


    }

    private void Init(){
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        btnRegister = findViewById(R.id.btn_register);
        register = findViewById(R.id.ll_register);
    }

    private void Register(){
        String email = edt_email.getText().toString();
        String password = edt_password.getText().toString();

        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
            String id = databaseReference.push().getKey();
            RegisterObject artist = new RegisterObject(id,email,password);

            databaseReference.child(id).setValue(artist);
            Toast.makeText(context, "Account Added", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "You should enter a name", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, MainActivity.class));
    }
}
