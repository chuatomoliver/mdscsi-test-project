package com.goldendevs.mdschallene.Models;


import com.goldendevs.mdschallene.Enum.FoodType;

public class Food {
    private FoodType foodType;
    private String name;
    private String description;
    private int price;
    private int thumbnail;

    //region Getter and Setter
    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
    //endregion

    // Constructor
    public Food(String name, String description, int price){
        this.name = name;
        this.description = description;
        this.price = price;
    }

}
