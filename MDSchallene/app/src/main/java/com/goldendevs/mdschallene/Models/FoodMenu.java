package com.goldendevs.mdschallene.Models;

public class FoodMenu {
    private String name;
    private String description;
    private int thumbnail;
    private int price;

    //region Getter and Setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
    //endregion

    // Constructor
    public FoodMenu(String name, String description, int thumbnail, int price){
        this.name  = name;
        this.description = description;
        this.thumbnail = thumbnail;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
