package com.goldendevs.mdschallene.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.goldendevs.mdschallene.fragments.HomePager1;
import com.goldendevs.mdschallene.fragments.HomePager2;
import com.goldendevs.mdschallene.fragments.HomePager3;

public class HomePageAdapter extends FragmentStatePagerAdapter {
    public HomePageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                HomePager1 tab1 = new HomePager1();
                return tab1;
            case 1:
                HomePager2 tab2 = new HomePager2();
                return tab2;
            case 2:
                HomePager3 tab3 = new HomePager3();
                return tab3;

            default:
                HomePager1 tab = new HomePager1();
                return tab;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
