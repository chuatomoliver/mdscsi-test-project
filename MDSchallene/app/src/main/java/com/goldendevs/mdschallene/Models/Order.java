package com.goldendevs.mdschallene.Models;

public class Order {


    private FoodMenu foodMenu;
    private int foodCount;
    private int drinkCount;
    private int extraCount;
    private int drinkPrice = 30;
    private int extraPrice = 20;


    public Order(FoodMenu foodMenu, int foodCount, int drinkCount, int extraCount){

        this.foodMenu = foodMenu;
        this.foodCount = foodCount;
        this.drinkCount = drinkCount;
        this.extraCount  = extraCount;
    }

    public FoodMenu getFoodMenu() {
        return foodMenu;
    }

    public void setFoodMenu(FoodMenu foodMenu) {
        this.foodMenu = foodMenu;
    }

    public int getFoodCount() {
        return foodCount;
    }

    public void setFoodCount(int foodCount) {
        this.foodCount = foodCount;
    }

    public int getDrinkCount() {
        return drinkCount;
    }

    public void setDrinkCount(int drinkCount) {
        this.drinkCount = drinkCount;
    }

    public int getExtraCount() {
        return extraCount;
    }

    public void setExtraCount(int extraCount) {
        this.extraCount = extraCount;
    }

    public int GetTotalAmount(){
        int  foodtotal = foodMenu.getPrice() * foodCount;
        int drinktotal = drinkPrice * drinkCount;
        int extraTotal = extraCount + extraPrice;
        return  foodtotal + drinktotal + extraTotal;
    }
}
