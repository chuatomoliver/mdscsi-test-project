package com.goldendevs.mdschallene.Enum;

public enum FoodType {
    MEAL,
    DRINK,
    EXTRA
}
