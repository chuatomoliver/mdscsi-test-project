package com.goldendevs.mdschallene;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.goldendevs.mdschallene.Adapter.MenuOrderAdapter;
import com.goldendevs.mdschallene.Models.BookOrder;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;


public class Home extends AppCompatActivity {
    CarouselView carouselView;
    TextView name;
    TextView email;
    Button logout;
    Context context = this;
    GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private ArrayList<BookOrder> bookOrderModelClasses;
    private RecyclerView recyclerView;
    MenuOrderAdapter menuOrderAdapter;



    private Integer image[] = {R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1,R.drawable.menu1};
    private String txt[]={"Mcdo","Mcdo","Mcdo","Mcdo","Mcdo","Mcdo Mcdo","Mcdo ","Mcdo"};
    private ImageView cart;

    // Images reference
    int[] carouseImagesDb = {
            R.drawable.menu1,
            R.drawable.menu1,
            R.drawable.menu1,
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initVariables();
        InitCarousel();
        name.setText(SingeltonOne.getInstance().name);
        email.setText(SingeltonOne.getInstance().email);

        recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        bookOrderModelClasses = new ArrayList<>();

        for (int i = 0; i < image.length; i++) {
            BookOrder beanClassForRecyclerView_contacts = new BookOrder(image[i],txt[i]);
            bookOrderModelClasses.add(beanClassForRecyclerView_contacts);
        }
        menuOrderAdapter = new MenuOrderAdapter(context,bookOrderModelClasses);
        recyclerView.setAdapter(menuOrderAdapter);

        cart = findViewById(R.id.cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Cart.class));
            }
        });

        if(SingeltonOne.getInstance().status == 1) {
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginManager.getInstance().logOut();
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                }
            });
        }
        if (SingeltonOne.getInstance().status == 2){
            // google
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   google();
                }
            });
        }

    }

    private void InitCarousel() {
        carouselView = findViewById(R.id.carouselView);
        carouselView.setPageCount(carouseImagesDb.length);

        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(carouseImagesDb[position]);
            }
        });
    }

    private void google(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);




        FirebaseAuth.getInstance().signOut();
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                task -> updateUI());
    }

    private void updateUI() {
        startActivity(new Intent(context, MainActivity.class));
        finish();
    }


    private void initVariables(){
        name = findViewById(R.id.tv_name);
        email = findViewById(R.id.tv_email);
        logout = findViewById(R.id.login_button);
        mAuth = FirebaseAuth.getInstance();
    }
}
