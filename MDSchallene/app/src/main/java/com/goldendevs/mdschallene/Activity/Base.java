package com.goldendevs.mdschallene.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;


public class Base extends AppCompatActivity {


    public String getApiUrl() {
        return getSharedPreferences("API_URL", Context.MODE_PRIVATE).getString("API_URL", "192.168.1.240");
    }

    public static String ENDPOINT = "/PNP/api/v1/index.php/";
}
