package com.goldendevs.mdschallene;

public class SingeltonOne {
    public static SingeltonOne singeltonInstance;

    public static SingeltonOne getInstance(){
        if(singeltonInstance == null){
            singeltonInstance = new SingeltonOne();
        }
        return singeltonInstance;
    }

    public String name;
    public String img_url;
    public String email;
    public int status;
    public String linkPGI;
}
