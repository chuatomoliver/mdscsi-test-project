package com.goldendevs.mdschallene.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import com.goldendevs.mdschallene.R;
import com.goldendevs.mdschallene.app.App;
import com.multidots.fingerprintauth.AuthErrorCodes;
import com.multidots.fingerprintauth.FingerPrintAuthCallback;
import com.multidots.fingerprintauth.FingerPrintAuthHelper;
import com.multidots.fingerprintauth.FingerPrintUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import customfonts.MyTextView_Lato_Regular;

public class FingerPrint extends Base implements FingerPrintAuthCallback {
    Context context = this;
    TextToSpeech t1;
    MyTextView_Lato_Regular tvText;

    private TextView mAuthMsgTv;
    private ViewSwitcher mSwitcher;
    private Button mGoToSettingsBtn;
    private FingerPrintAuthHelper mFingerPrintAuthHelper;
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_print);
        InitVariables();
        textSpeech();

        String response = "Please enter your fingerprint";
        Toast.makeText(this, "Please enter your fingerprint", Toast.LENGTH_SHORT).show();
        t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);

        mGoToSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FingerPrintUtils.openSecuritySettings(context);
            }
        });
        mFingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(this, this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void InitVariables(){
        tvText = (MyTextView_Lato_Regular)findViewById(R.id.lbl_finger_print);
        mGoToSettingsBtn = (Button) findViewById(R.id.go_to_settings_btn);
        mSwitcher = (ViewSwitcher) findViewById(R.id.main_switcher);
        mAuthMsgTv = (TextView) findViewById(R.id.auth_message_tv);
        EditText pinEt = (EditText) findViewById(R.id.pin_et);
        back = (ImageView)findViewById(R.id.navigation_back);

    }

    private void textSpeech(){
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    Locale locale = new Locale("en","US");
                    Locale.setDefault(locale);
                    t1.setLanguage(locale);
                }
            }
        });
    }











    @Override
    public void onNoFingerPrintHardwareFound() {
        mAuthMsgTv.setText("Your device does not have finger print scanner. Please type 1234 to authenticate.");
        mSwitcher.showNext();
    }

    @Override
    public void onNoFingerPrintRegistered() {
        mAuthMsgTv.setText("There are no finger prints registered on this device. Please register your finger from settings.");
        mGoToSettingsBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBelowMarshmallow() {
        mAuthMsgTv.setText("You are running older version of android that does not support finger print authentication. Please type 1234 to authenticate.");
        mSwitcher.showNext();
    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        showDialogSuccess();
    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        switch (errorCode) {
            case AuthErrorCodes.CANNOT_RECOGNIZE_ERROR:
                mAuthMsgTv.setText("Cannot recognize your finger print. Please try again.");
                break;
            case AuthErrorCodes.NON_RECOVERABLE_ERROR:
                mAuthMsgTv.setText("Cannot initialize finger print authentication. Please type 1234 to authenticate.");
                mSwitcher.showNext();
                break;
            case AuthErrorCodes.RECOVERABLE_ERROR:
                mAuthMsgTv.setText(errorMessage);
                break;
        }
    }










    @Override
    protected void onResume() {
        super.onResume();
        mGoToSettingsBtn.setVisibility(View.GONE);

        mAuthMsgTv.setText("Scan your finger");

        //start finger print authentication
        mFingerPrintAuthHelper.startAuth();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(t1 != null){

            t1.stop();
            t1.shutdown();
        }
        mFingerPrintAuthHelper.stopAuth();
    }

    private void showDialogSuccess() {
        String response = "Proceed to Drug Text History";
        t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
        final Dialog dialog = new Dialog(this, android.R.style.Theme_NoTitleBar_Fullscreen);
            dialog.setContentView(R.layout.dialog_finger_print_success);
        TextView btnRegistered = dialog.findViewById(R.id.txtRegistered);

        btnRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SaveRecords();
                saveDrugHistory();

            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void SaveRecords() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://"+getApiUrl()+"/PNP/api/v1/index.php/saveDrugTest", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    Toast.makeText(context, "response", Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(response);
                    if(obj.getString("error").equals("false")) {


                    }
                    else{
                        Toast.makeText(context, "Please check username and passw", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Iwatcher", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("first_name", "jeffrey");
                params.put("last_name", "valledo");
                params.put("age", "30");
                params.put("gender", "male");
                params.put("birthday", "august 09 1989");
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }

    public void saveDrugHistory() {
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://"+getApiUrl()+""+ ENDPOINT + "saveDrugHistory", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Toast.makeText(context, "response", Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(response);
                    if(obj.getString("error").equals("false")) {
//                        startActivity(new Intent(context, DrugTestHistory.class));
                        Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(context, "Please check username and passw", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Iwatcher", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        JSONObject object = new JSONObject(res);
//                        showDialog("Error", object.getString("message"), SweetAlertDialog.ERROR_TYPE);
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("clinic_name", "SULU PROVINCIAL HOSPITAL");
                params.put("date", "Oct 15 2019");
                params.put("status", "pending");
                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToReqQueue(postRequest);
    }
}
