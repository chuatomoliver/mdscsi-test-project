package com.goldendevs.mdschallene.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.goldendevs.mdschallene.Models.Nav_3_ModalClass;
import com.goldendevs.mdschallene.R;


/**
 * Created by praja on 23-05-17.
 */

public class Navigation_3_Adapter extends RecyclerView.Adapter<Navigation_3_Adapter.MyViewHolder>{


public Context mContext;
    public Nav_3_ModalClass[] nav_3_modalClasses;

    public Navigation_3_Adapter(Context mContext, Nav_3_ModalClass[] nav_3_modalClasses) {
        this.mContext = mContext;
        this.nav_3_modalClasses = nav_3_modalClasses;
    }

public class MyViewHolder extends RecyclerView.ViewHolder{

    public TextView txtname,txtcount;
    public ImageView img;

    public MyViewHolder(View itemView) {
        super(itemView);

        txtname = (TextView)itemView.findViewById(R.id.txtname);
        txtcount = (TextView)itemView.findViewById(R.id.txtcount);
        img = (ImageView)itemView.findViewById(R.id.img);

    }
}


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nav_3, parent, false);
        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtname.setText(nav_3_modalClasses[position].getName());
        holder.txtcount.setText(nav_3_modalClasses[position].getCount());
        holder.img.setImageResource(nav_3_modalClasses[position].getImage());


        if (position == 2){

            holder.txtcount.setBackground(mContext.getResources().getDrawable(R.drawable.round_rect));
        }else if (position == 3){

            holder.txtcount.setBackground(mContext.getResources().getDrawable(R.drawable.round_rect));

        }

    }

    @Override
    public int getItemCount() {
        return nav_3_modalClasses.length;
    }


}
