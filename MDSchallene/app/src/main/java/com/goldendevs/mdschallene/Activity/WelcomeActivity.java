package com.goldendevs.mdschallene.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.login.LoginManager;
import com.goldendevs.mdschallene.Adapter.MenuOrderAdapter;
import com.goldendevs.mdschallene.Adapter.NavigationMenuAdapter;
import com.goldendevs.mdschallene.Adapter.ViewPageAdapter;
import com.goldendevs.mdschallene.MainActivity;
import com.goldendevs.mdschallene.Models.BookOrder;
import com.goldendevs.mdschallene.Models.Nav3ModelClass;
import com.goldendevs.mdschallene.R;
import com.goldendevs.mdschallene.SingeltonOne;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Locale;

import customfonts.MyTextView_Lato_Medium;
import de.hdodenhof.circleimageview.CircleImageView;
import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;

public class WelcomeActivity extends Base implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, RecognitionListener {

    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private String LOG_TAG = "VoiceRecognitionActivity";
    // BottomBar
    private LinearLayout linearLayout_one, linearLayout_two, linearLayout_three, linearLayout_four, linearLayout_five;
    private ImageView imageView_one, imageView_two, imageView_three, imageView_four, imageView_five;
    private TextView textView_one, textView_two, textView_four, textView_five;

    // Fragments
    ViewPageAdapter viewPageAdapter;
    ViewPager viewPager;

    String voicetText = "";
    //nav
    private DuoDrawerLayout drawerLayout;
    private Toolbar toolbar;
    NavigationView navigationView;
    Context context = this;
    NavigationMenuAdapter navigationMenuAdapter;
    RecyclerView recycleview;


    private ProgressBar progressBar;
    private ToggleButton BtnVoiceRecognition;
    TextToSpeech t1;
    // navigation menu

    Nav3ModelClass[] nav3ModelClasses = {
            new Nav3ModelClass(R.drawable.ic_account_circle_black_24dp, "PROFILE", ""),
            new Nav3ModelClass(R.drawable.ic_home_black_24dp, "ACTIVITY", ""),
            new Nav3ModelClass(R.drawable.ic_assignment_black_24dp, "LEGENDS", "10"),
            new Nav3ModelClass(R.drawable.ic_exit_to_app_black_24dp, "LOGOUT", "")

    };


    TextView name;
    TextView email;
    Button logout;
    GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private ArrayList<BookOrder> bookOrderModelClasses;
    private RecyclerView recyclerView;
    MenuOrderAdapter menuOrderAdapter;
    private ImageView circleImageView;
    private MyTextView_Lato_Medium profile_name;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_activity);
        InitToolbar();
        InitNavbar();
        SpeechTrigger();
        profile_name = findViewById(R.id.name);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.dontAnimate();

        BtnVoiceRecognition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {

                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    speech.startListening(recognizerIntent);
                } else {
                    progressBar.setIndeterminate(false);
                    progressBar.setVisibility(View.INVISIBLE);
                    speech.stopListening();
                }
            }
        });

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    Locale locale = new Locale("tl_PH","Philippines");
                    Locale.setDefault(locale);
                    t1.setLanguage(locale);
                }
            }
        });




        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager(),context);
        viewPager.setAdapter(viewPageAdapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    clickWalls();
                }
                if (position == 1){
                    clickTags();
                }
                if (position == 2) {
                    clickFavorites();
                }
                if (position == 3){
                    clickProfile();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        toolbar.findViewById(R.id.navigation_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer();
            }
        });




        navigationMenuAdapter = new NavigationMenuAdapter(context, nav3ModelClasses);
        recycleview.setLayoutManager(new LinearLayoutManager(context));
        recycleview.setAdapter(navigationMenuAdapter);


        linearLayout_one.setOnClickListener(this);
        linearLayout_two.setOnClickListener(this);
        linearLayout_three.setOnClickListener(this);
        linearLayout_four.setOnClickListener(this);
        linearLayout_five.setOnClickListener(this);


        if(SingeltonOne.getInstance().status == 1) {
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginManager.getInstance().logOut();
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                }
            });
        }
        if (SingeltonOne.getInstance().status == 2){
            // google
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    google();
                }
            });
        }
    }

    private void InitNavbar(){
        drawerLayout =  findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view1);
        recycleview =  findViewById(R.id.recycleview);
        navigationView.bringToFront();
        logout = findViewById(R.id.login_button);

        BtnVoiceRecognition = (ToggleButton)findViewById(R.id.camera_img);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
    }

    private void InitToolbar(){
        linearLayout_one =  findViewById(R.id.walls);
        linearLayout_two =  findViewById(R.id.tags);
        linearLayout_three =  findViewById(R.id.camera);
        linearLayout_four =  findViewById(R.id.favorites1);
        linearLayout_five =  findViewById(R.id.profile);


        imageView_one =  findViewById(R.id.walls_img);
        imageView_two =  findViewById(R.id.tags_img);
//        imageView_three = findViewById(R.id.camera_img);
        imageView_four =  findViewById(R.id.favorites_img);
        imageView_five =  findViewById(R.id.profile_img);

        textView_one =  findViewById(R.id.walls_txt);
        textView_two =  findViewById(R.id.tags_txt);
        textView_four =  findViewById(R.id.favorites_txt);
        textView_five =  findViewById(R.id.profile_txt);

        // viewpager
        viewPager = findViewById(R.id.view_pager);
        toolbar = findViewById(R.id.toolbar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.walls:
                    clickWalls();
                    viewPager.setCurrentItem(0);
                break;
            case R.id.tags:
                    clickTags();
                    viewPager.setCurrentItem(1);
                break;
            case R.id.camera:
                    imageView_three.setImageResource(R.drawable.ic_mic_white_24dp);
                    Toast.makeText(this, "Cam", Toast.LENGTH_SHORT).show();
                break;
            case R.id.favorites1:
                    clickFavorites();
                    viewPager.setCurrentItem(2);
                break;
            case R.id.profile:
                    clickProfile();
                     viewPager.setCurrentItem(3);
                break;
        }
    }



    private void clickWalls(){
        imageView_one.setImageResource(R.drawable.ic_mchat);
        imageView_two.setImageResource(R.drawable.ic_wpricetag);
//        imageView_three=findViewById(R.id.camera_img);
        imageView_four.setImageResource(R.drawable.ic_wstar);
        imageView_five.setImageResource(R.drawable.ic_wprofile);

        textView_one.setTextColor(getResources().getColor(R.color.maroon));
        textView_two.setTextColor(getResources().getColor(R.color.white));
        textView_four.setTextColor(getResources().getColor(R.color.white));
        textView_five.setTextColor(getResources().getColor(R.color.white));
    }

    private void clickTags(){
        imageView_one.setImageResource(R.drawable.ic_wchat);
        imageView_two.setImageResource(R.drawable.ic_mpricetag);
//        imageView_three=findViewById(R.id.camera_img);
        imageView_four.setImageResource(R.drawable.ic_wstar);
        imageView_five.setImageResource(R.drawable.ic_wprofile);

        textView_one.setTextColor(getResources().getColor(R.color.white));
        textView_two.setTextColor(getResources().getColor(R.color.maroon));
        textView_four.setTextColor(getResources().getColor(R.color.white));
        textView_five.setTextColor(getResources().getColor(R.color.white));
    }

    private void clickFavorites(){
        imageView_one.setImageResource(R.drawable.ic_wchat);
        imageView_two.setImageResource(R.drawable.ic_wpricetag);
//        imageView_three=findViewById(R.id.camera_img);
        imageView_four.setImageResource(R.drawable.ic_mstar);
        imageView_five.setImageResource(R.drawable.ic_wprofile);

        textView_one.setTextColor(getResources().getColor(R.color.white));
        textView_two.setTextColor(getResources().getColor(R.color.white));
        textView_four.setTextColor(getResources().getColor(R.color.maroon));
        textView_five.setTextColor(getResources().getColor(R.color.white));
    }

    private void clickProfile(){
        imageView_one.setImageResource(R.drawable.ic_wchat);
        imageView_two.setImageResource(R.drawable.ic_wpricetag);
//        imageView_three=findViewById(R.id.camera_img);
        imageView_four.setImageResource(R.drawable.ic_wstar);
        imageView_five.setImageResource(R.drawable.ic_mprofile);

        textView_one.setTextColor(getResources().getColor(R.color.white));
        textView_two.setTextColor(getResources().getColor(R.color.white));
        textView_four.setTextColor(getResources().getColor(R.color.white));
        textView_five.setTextColor(getResources().getColor(R.color.maroon));
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");
        progressBar.setIndeterminate(false);
        progressBar.setMax(3);
    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(LOG_TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(LOG_TAG, "onEndOfSpeech");
        progressBar.setIndeterminate(true);
        BtnVoiceRecognition.setChecked(false);
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(LOG_TAG, "FAILED " + errorMessage);
        Toast.makeText(context, "" + errorMessage.toString(), Toast.LENGTH_SHORT).show();
        BtnVoiceRecognition.setChecked(false);
    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {

        Log.i(LOG_TAG, "onEvent");
    }

    @Override
    public void onPartialResults(Bundle arg0) {

        Log.i(LOG_TAG, "onPartialResults");
    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(LOG_TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text += result + "\n";


        voicetText = matches.get(0).toString();
        Toast.makeText(context, "" +  matches.get(0), Toast.LENGTH_SHORT).show();
        if(voicetText.contains("test")){
            String response = "Kamusta Sayo. Anong maitutulong ko";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
        }

        if(voicetText.contains("profile")){
            String response = "Ang application ay papunta na sa profile";
            startActivity(new Intent(context, Profile.class));
        }

        if(voicetText.contains("event")){
            String response = "Ang application ay papunta na sa report";
            startActivity(new Intent(context, BarcodeSuccess.class));
        }

        if(voicetText.contains("image")){
            String response = "Ang application ay papunta na sa report";
            startActivity(new Intent(context, MVReport.class));
        }

        if(voicetText.contains("drug test")){
            String response = "Ang application ay papunta na sa report";
            startActivity(new Intent(context, FingerPrint.class));
        }

        if(voicetText.contains("assistant")){
            String response = "Ang application ay tinatawag na ang assitant";
//            startActivity(new Intent(context, DrugTestHistory.class));
        }


        if(voicetText.contains("emergency")){
            String response = "Darating na iyong kasamahan. Mag antay lang saglit";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);

        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void google(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);




        FirebaseAuth.getInstance().signOut();
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                task -> updateUI());
    }

    private void updateUI() {
        startActivity(new Intent(context, MainActivity.class));
        finish();
    }

    private void SpeechTrigger(){
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,"fil_PH");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
    }



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null) {
            speech.destroy();
            Log.i(LOG_TAG, "destroy");
        }

        if(t1 != null){

            t1.stop();
            t1.shutdown();
        }
        super.onPause();

    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }
}


