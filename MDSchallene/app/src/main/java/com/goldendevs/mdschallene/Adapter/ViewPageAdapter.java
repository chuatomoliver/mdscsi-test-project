package com.goldendevs.mdschallene.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.goldendevs.mdschallene.fragments.FirstFragment;
import com.goldendevs.mdschallene.fragments.FourthFragment;
import com.goldendevs.mdschallene.fragments.SecondFragment;
import com.goldendevs.mdschallene.fragments.ThirdFragment;


public class ViewPageAdapter extends FragmentPagerAdapter {
    private Context context;


    public ViewPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {
            case 0:
                return FirstFragment.newInstance(1);
            case 1:
                return SecondFragment.newInstance(2);
            case 2:
                return ThirdFragment.newInstance(3);
            case 3:
                return FourthFragment.newInstance(4);
        }

        return null;
    }

    @Override
    public int getCount() {
        int PAGE_COUNT = 4;
        return PAGE_COUNT;
    }


}
